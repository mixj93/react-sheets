import * as React from 'react'
import ReactDOM from 'react-dom'
import { observer } from 'mobx-react'
import { observable, toJS, computed } from 'mobx'
import {
  ISheetTableProps,
  ISheetTableCell,
  ESheetTableCellDataType,
  ESheetTableSortDataType,
  ESheetTableSortOrderType,
  ICellCord,
  ISheetTableCells
} from './interface'
import {
  getTopHeaderTitles,
  getLeftHeaderTitles,
  tryTransToNumber,
  initCells,
  getCellsFromRow,
  getCellsFromCol,
  aToZSorter,
  zToASorter,
  numberToLetters
} from './util'
import Menu from './Menu'
import './index.css'
import FormulaBar from './FormulaBar'
import { isNumber } from 'util'
import { INIT_CELL_DATA } from './const'

const FormulaParser = require('hot-formula-parser').Parser
const parser = new FormulaParser()

@observer
class SheetTable extends React.Component<ISheetTableProps, {}> {
  // @observable.array cells: ISheetTableCell[][] = []
  @observable cells: ISheetTableCells = {}
  @observable choosedCell = { rowTitle: '1', colTitle: 'A', canEdit: false }
  @observable choosedRowTitle: string = ''
  @observable choosedColTitle: string = ''
  @observable editorText: string = ''
  @observable formulaText: string = ''
  @observable menu: { show: boolean; type: ESheetTableSortDataType; top: number; left: number } = {
    show: false,
    type: 'row',
    top: 0,
    left: 0
  }

  constructor(props: ISheetTableProps) {
    super(props)
    const { row, col } = props
    // * init cell content
    this.cells = initCells(row, col)
  }

  isCellChoosed = (rowTitle: string, colTitle: string) => {
    return this.choosedCell && this.choosedCell.rowTitle === rowTitle && this.choosedCell.colTitle === colTitle
  }

  canCellEdit = (rowTitle: string, colTitle: string) => {
    return this.isCellChoosed(rowTitle, colTitle) && this.choosedCell.canEdit
  }

  areCellsEqual = (cellX: ISheetTableCell, cellY: ISheetTableCell): boolean => {
    return cellX.content === cellY.content && cellX.formula === cellY.formula
  }

  @computed get choosedCellData() {
    const rowTitle = this.choosedCell.rowTitle
    return (this.cells[rowTitle] && this.cells[rowTitle][this.choosedCell.colTitle]) || null
  }

  getCellContent = (rowTitle: string, colTitle: string): string | number => {
    let ret: string | number = ''
    if (this.cells[rowTitle] && this.cells[rowTitle][colTitle]) {
      const content = this.cells[rowTitle][colTitle].content
      if (content === 0) {
        ret = 0
      } else {
        ret = content || ''
      }
    }

    return ret
  }

  getCellFormula = (rowTitle: string, colTitle: string): string => {
    return (this.cells[rowTitle] && this.cells[rowTitle][colTitle] && this.cells[rowTitle][colTitle].formula) || ''
  }

  // getCellDisplay = (rowTitle: string, colTitle: string): string | number => {
  //   let ret
  //   const formula = this.getCellFormula(rowTitle, colTitle)
  //   if (formula) {
  //     const parserObj = parser.parse(formula)
  //     ret = parserObj.result || parserObj.error
  //   } else {
  //     ret = this.getCellContent(rowTitle, colTitle)
  //   }
  //   return ret
  // }

  getCellDataType = (rowTitle: string, colTitle: string): ESheetTableCellDataType => {
    return isNumber(this.getCellContent(rowTitle, colTitle)) ? 'number' : 'string'
  }

  getCellClassName = (rowTitle: string, colTitle: string) => {
    let cellClassName = 'cell'
    if (this.isCellChoosed(rowTitle, colTitle)) {
      cellClassName += ' choosed'
    }
    if (this.canCellEdit(rowTitle, colTitle)) {
      cellClassName += ' edited'
    }
    if (this.choosedRowTitle === rowTitle) {
      cellClassName += ' choosed-row'
    }
    if (this.choosedColTitle === colTitle) {
      cellClassName += ' choosed-col'
    }
    cellClassName += ` ${this.getCellDataType(rowTitle, colTitle)}`

    return cellClassName
  }

  getMenuPos = (clientX: number, clientY: number) => {
    var tableEle = ReactDOM.findDOMNode(this) as any
    return {
      top: clientY - tableEle.offsetTop,
      left: clientX - tableEle.offsetLeft
    }
  }

  getContentFromFormula = (formula: string) => {
    const parserObj = parser.parse(formula)
    return parserObj.result || parserObj.error
  }

  setEditInput = (text: string) => {
    if (this.choosedCell.canEdit) {
      this.setCellContent(this.choosedCell.rowTitle, this.choosedCell.colTitle, tryTransToNumber(text))
      this.editorText = ''
      this.refreshCellsContent(this.choosedCell.rowTitle, this.choosedCell.colTitle)
    }
  }

  showMenu = (clientX: number, clientY: number, type: ESheetTableSortDataType) => {
    const pos = this.getMenuPos(clientX, clientY)
    this.menu = {
      show: true,
      type,
      top: pos.top,
      left: pos.left
    }
  }

  hideMenu = () => {
    this.menu.show = false
  }

  chooseCell = (rowTitle: string, colTitle: string) => {
    if (this.choosedCell.rowTitle !== rowTitle || this.choosedCell.colTitle !== colTitle) {
      this.choosedCell = {
        rowTitle,
        colTitle,
        canEdit: false
      }
      this.formulaText = this.choosedCellData.formula
    }
  }

  setCellFormula = (rowTitle: string, colTitle: string, formula: string) => {
    if (this.cells[rowTitle] && this.cells[rowTitle][colTitle]) {
      this.cells[rowTitle][colTitle].formula = formula
    }
  }

  setCellContent = (rowTitle: string, colTitle: string, content: string | number) => {
    if (this.cells[rowTitle] && this.cells[rowTitle][colTitle]) {
      this.cells[rowTitle][colTitle].content = content
    }
  }

  handleCellClick = (rowTitle: string, colTitle: string) => {
    this.hideMenu()
    if (!this.isCellChoosed(rowTitle, colTitle)) {
      // * reset choosedRowTitle and choosedColTitle
      this.choosedRowTitle = ''
      this.choosedColTitle = ''

      // * set edit last cell content
      this.setEditInput(this.editorText)

      // * choose next cell
      this.chooseCell(rowTitle, colTitle)
    }
  }

  handleCellDoubleClick = (rowTitle: string, colTitle: string) => {
    this.hideMenu()
    this.choosedCell = {
      rowTitle,
      colTitle,
      canEdit: true
    }
    this.editorText = String(this.getCellContent(rowTitle, colTitle)) || ''
  }

  handleCellContextMenu = (e: any, rowTitle: string, colTitle: string) => {
    e.preventDefault()
    this.hideMenu()
    this.handleCellClick(rowTitle, colTitle)
  }

  handleTopHeaderContextMenu = (e: any, colTitle: string) => {
    e.preventDefault()
    this.setEditInput(this.editorText)
    // * reset
    this.hideMenu()
    this.choosedRowTitle = ''
    this.choosedColTitle = colTitle

    this.chooseCell('1', colTitle)

    this.showMenu(e.clientX, e.clientY, 'col')
  }

  handleLeftHeaderContextMenu = (e: any, rowTitle: string) => {
    e.preventDefault()
    this.setEditInput(this.editorText)
    // * reset
    this.hideMenu()
    this.choosedColTitle = ''
    this.choosedRowTitle = rowTitle

    this.chooseCell(rowTitle, 'A')

    this.showMenu(e.clientX, e.clientY, 'row')
  }

  handleCellEditorChange = (e: any) => {
    this.setCellFormula(this.choosedCell.rowTitle, this.choosedCell.colTitle, '')
    this.formulaText = ''
    this.editorText = e.target.value
  }

  handleSortAToZ = () => {
    this.sortAndSetCells('a-z')
    this.hideMenu()
  }

  handleSortZToA = () => {
    this.sortAndSetCells('z-a')
    this.hideMenu()
  }

  handleFormulaInputChange = (e: any) => {
    this.formulaText = e.target.value
  }

  handleFormulaSave = () => {
    this.setCellFormula(this.choosedCell.rowTitle, this.choosedCell.colTitle, this.formulaText)
    this.setCellContent(
      this.choosedCell.rowTitle,
      this.choosedCell.colTitle,
      this.getContentFromFormula(this.formulaText)
    )
    this.refreshCellsContent(this.choosedCell.rowTitle, this.choosedCell.colTitle)
  }

  // * cell's change will effect other cells
  refreshCellsContent = (rowTitle: string, colTitle: string) => {
    const label = `${colTitle}${rowTitle}`.toLowerCase()
    for (const r in this.cells) {
      if (this.cells.hasOwnProperty(r)) {
        const row = this.cells[r]
        for (const c in row) {
          if (row.hasOwnProperty(c)) {
            const cell = row[c]
            if (cell.formula.toLowerCase().indexOf(label) >= 0) {
              this.cells[r][c].content = this.getContentFromFormula(cell.formula)
              this.refreshCellsContent(r, c)
            }
          }
        }
      }
    }
  }

  sortAndSetCells = (sortOrder: ESheetTableSortOrderType) => {
    const sortingCells =
      this.menu.type === 'row'
        ? getCellsFromRow(toJS(this.cells), this.choosedRowTitle)
        : getCellsFromCol(toJS(this.cells), this.choosedColTitle)

    const sortCellsArr = Object.keys(sortingCells).map(key => sortingCells[key])
    sortCellsArr.sort((x, y) => {
      const displayX = x.formula ? parser.parse(x.formula).result || parser.parse(x.formula).error : x.content
      const displayY = y.formula ? parser.parse(y.formula).result || parser.parse(y.formula).error : y.content
      if (sortOrder === 'a-z') {
        return aToZSorter(displayX, displayY)
      } else {
        return zToASorter(displayX, displayY)
      }
    })

    const sortedCells: any = {}
    sortCellsArr.forEach((item, index) => {
      if (this.menu.type === 'row') {
        sortedCells[numberToLetters(index + 1)] = item
      } else {
        sortedCells[String(index + 1)] = item
      }
    })
    if (this.menu.type === 'row') {
      for (const colTitle in this.cells[this.choosedRowTitle]) {
        if (this.cells[this.choosedRowTitle].hasOwnProperty(colTitle)) {
          this.cells[this.choosedRowTitle][colTitle] = sortedCells[colTitle] || INIT_CELL_DATA
        }
      }
    } else {
      for (const rowTitle in this.cells) {
        if (this.cells.hasOwnProperty(rowTitle) && this.cells[rowTitle]) {
          this.cells[rowTitle][this.choosedColTitle] = sortedCells[rowTitle] || INIT_CELL_DATA
        }
      }
    }
  }

  componentDidMount = () => {
    // * init parser
    parser.on('callCellValue', (cellCoord: ICellCord, done: any) => {
      if (!cellCoord.label) {
        return
      }
      const rowTitle = cellCoord.row.label
      const colTitle = cellCoord.column.label
      const display = this.getCellContent(rowTitle, colTitle)
      done(display)
    })
  }

  render() {
    const { row, col } = this.props
    return (
      <div className="SheetTable">
        <FormulaBar
          rowTitle={this.choosedCell.rowTitle}
          colTitle={this.choosedCell.colTitle}
          value={this.formulaText}
          onChange={this.handleFormulaInputChange}
          onSave={this.handleFormulaSave}
        />

        <div className="wrapper">
          <div className="table" style={{ width: `${45 + col * 100}px`, height: `${(col + 1) * 24}px` }}>
            <div className="top-header clearfix">
              <div className="cell header top left" />

              <div className="cell-group">
                {getTopHeaderTitles(col).map(title => (
                  <div
                    className={`cell header top ${this.choosedCell.colTitle === title ? 'choosed' : ''}`}
                    key={title}
                    onContextMenu={e => this.handleTopHeaderContextMenu(e, title)}
                  >
                    {title}
                  </div>
                ))}
              </div>
            </div>

            <div className="bottom-content clearfix">
              <div className="left-header">
                <div className="cell-group">
                  {getLeftHeaderTitles(row).map(title => (
                    <div
                      className={`cell header left ${this.choosedCell.rowTitle === title ? 'choosed' : ''}`}
                      key={title}
                      onContextMenu={e => this.handleLeftHeaderContextMenu(e, title)}
                    >
                      {title}
                    </div>
                  ))}
                </div>
              </div>
              <div className="body">
                <div
                  className="cell-group"
                  style={{ gridTemplateColumns: `repeat(${col}, 100px)`, gridTemplateRows: `repeat(${col}, 24px)` }}
                >
                  {getLeftHeaderTitles(row).map(cellRow => {
                    return getTopHeaderTitles(col).map(cellCol => {
                      return (
                        <div
                          className={this.getCellClassName(cellRow, cellCol)}
                          key={`${cellCol}-${cellRow}`}
                          onClick={() => this.handleCellClick(cellRow, cellCol)}
                          onContextMenu={e => this.handleCellContextMenu(e, cellRow, cellCol)}
                          onDoubleClick={() => this.handleCellDoubleClick(cellRow, cellCol)}
                        >
                          {this.canCellEdit(cellRow, cellCol) ? (
                            <input
                              className="cell-editor"
                              autoFocus
                              // onChange={e => this.handleCellEditorChange(e)}
                              onChange={this.handleCellEditorChange}
                              defaultValue={String(this.getCellContent(cellRow, cellCol))}
                            />
                          ) : (
                            this.getCellContent(cellRow, cellCol)
                          )}
                        </div>
                      )
                    })
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>

        {this.menu.show && (
          <Menu top={this.menu.top} left={this.menu.left} sortAZ={this.handleSortAToZ} sortZA={this.handleSortZToA} />
        )}
      </div>
    )
  }
}

export default SheetTable
