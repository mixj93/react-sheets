import { INIT_CELL_DATA } from './const'
import { ISheetTableCell, ISheetTableCells } from './interface'

export const initCells = (row: number, col: number): ISheetTableCells => {
  const cells: ISheetTableCells = {}
  const rowCells: any = {}
  for (let c = 1; c <= col; c++) {
    rowCells[numberToLetters(c)] = INIT_CELL_DATA
  }
  for (let r = 1; r <= row; r++) {
    cells[String(r)] = rowCells
  }

  return cells
}

export const lettersToNumber = (letters: string): number => {
  var chrs = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    mode = chrs.length - 1,
    number = 0
  for (var p = 0; p < letters.length; p++) {
    number = number * mode + chrs.indexOf(letters[p])
  }
  return number
}

export const numberToLetters = (num: number): string => {
  var numeric = (num - 1) % 26
  var letter = chr(65 + numeric)
  var number2 = parseInt(String((num - 1) / 26))
  if (number2 > 0) {
    return numberToLetters(number2) + letter
  } else {
    return letter
  }
}

const chr = (codePt: number): string => {
  if (codePt > 0xffff) {
    codePt -= 0x10000
    return String.fromCharCode(0xd800 + (codePt >> 10), 0xdc00 + (codePt & 0x3ff))
  }
  return String.fromCharCode(codePt)
}

export const getLeftHeaderTitles = (num: number): string[] => {
  const arr: string[] = []
  for (let i = 1; i <= num; i++) {
    arr.push(String(i))
  }
  return arr
}

export const getTopHeaderTitles = (num: number) => {
  const arr: string[] = []
  for (let i = 1; i <= num; i++) {
    arr.push(numberToLetters(i))
  }
  return arr
}

export const isStringNumber = (str: string): boolean => {
  const num = parseFloat(str)
  return String(num) === str
}

export const tryTransToNumber = (str: string): number | string => {
  return isStringNumber(str) ? parseFloat(str) : str
}

const isNumber = (input: any): boolean => {
  return typeof input === 'number'
}

const isEmptyString = (input: any): boolean => {
  return input === ''
}

const basicSorter = (displayX: string | number, displayY: string | number) => {
  if (isEmptyString(displayX) && !isEmptyString(displayY)) {
    return 1
  } else if (!isEmptyString(displayX) && isEmptyString(displayY)) {
    return -1
  }
  if (isNumber(displayX) && !isNumber(displayY)) {
    return -1
  } else if (!isNumber(displayX) && isNumber(displayY)) {
    return 1
  }
  if (displayX < displayY) {
    return -1
  }
  if (displayX > displayY) {
    return 1
  }
  return 0
}

export const aToZSorter = (displayX: string | number, displayY: string | number) => {
  if (isEmptyString(displayX) && !isEmptyString(displayY)) {
    return 1
  } else if (!isEmptyString(displayX) && isEmptyString(displayY)) {
    return -1
  }
  return basicSorter(displayX, displayY)
}

export const zToASorter = (displayX: string | number, displayY: string | number) => {
  if (isEmptyString(displayX) && !isEmptyString(displayY)) {
    return 1
  } else if (!isEmptyString(displayX) && isEmptyString(displayY)) {
    return -1
  }
  return basicSorter(displayX, displayY) * -1
}

export const getCellsFromRow = (cells: ISheetTableCells, rowTitle: string): { [title: string]: ISheetTableCell } => {
  return cells[rowTitle]
}

export const getCellsFromCol = (cells: ISheetTableCells, colTitle: string): { [title: string]: ISheetTableCell } => {
  const ret: any = {}
  for (const rowTitle in cells) {
    if (cells.hasOwnProperty(rowTitle)) {
      const cell = cells[rowTitle][colTitle] || INIT_CELL_DATA
      ret[rowTitle] = cell
    }
  }

  return ret
}
