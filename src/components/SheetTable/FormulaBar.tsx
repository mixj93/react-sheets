import * as React from 'react'
import { IFormulaBarProps } from './interface'
import './FormulaBar.css'

const FormulaBar: React.SFC<IFormulaBarProps> = props => {
  const { rowTitle, colTitle, value, onChange, onSave } = props
  return (
    <div className="FormulaBar">
      <span className="fx">fx</span>
      <span className="cell-title">{`${colTitle}${rowTitle}`} =</span>
      <div className="input-wrap">
        <input className="input" value={value} onChange={onChange} />
        <span className="error">Formula is invalid.</span>
      </div>
      <button className="btn" onClick={onSave}>
        Save
      </button>
    </div>
  )
}

export default FormulaBar
