export type ESheetTableCellDataType = 'string' | 'number'
export type ESheetTableSortDataType = 'row' | 'col'
export type ESheetTableSortOrderType = 'a-z' | 'z-a'

export interface ISheetTableProps {
  row: number
  col: number
}

export interface ISheetTableCell {
  content: string | number
  formula: string
}

export interface ISheetTableCells {
  [row: string]: { [col: string]: ISheetTableCell }
}

export interface ISheetTableMenuProps {
  top: number
  left: number
  sortAZ: () => any
  sortZA: () => any
}

export interface IFormulaBarProps {
  rowTitle: string
  colTitle: string
  value: string
  onChange: (e: any) => any
  onSave: () => any
}

export interface ICellCord {
  column: {
    index: number
    isAbsolute: boolean
    label: string
  }
  label: string
  row: {
    index: number
    isAbsolute: boolean
    label: string
  }
}
