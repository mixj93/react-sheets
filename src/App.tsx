import React from 'react'
import './App.css'
import SheetTable from './components/SheetTable'

const App: React.SFC = () => {
  return (
    <div className="App">
      <h1 className="title">React Sheets Demo</h1>
      <div className="content">
        <SheetTable row={30} col={20} />
      </div>
      <div className="footer">
        <a className="link" target="_blank" rel="noopener noreferrer" href="https://gitlab.com/mixj93/react-sheets">
          View on Gitlab
        </a>
      </div>
    </div>
  )
}

export default App
