# React Sheets

Demo: [https://react-sheets.netlify.com](https://react-sheets.netlify.com)

## Usage

- Edit cell: double click cell to enter content.
- Sort col / sort row: Right click col or row header to sort from A-Z or Z-A.
- Formula Calculation

## Development

```
$ yarn add
$ yarn start
```
