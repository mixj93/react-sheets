import * as React from 'react'
import './Menu.css'
import { ISheetTableMenuProps } from './interface'

const Menu: React.SFC<ISheetTableMenuProps> = (props: ISheetTableMenuProps) => {
  const { top, left, sortAZ, sortZA } = props
  return (
    <div className="Menu" style={{ top, left }}>
      <ul>
        <li>
          <span className="item" onClick={sortAZ}>
            Sort A-Z
          </span>
        </li>
        <li>
          <span className="item" onClick={sortZA}>
            Sort Z-A
          </span>
        </li>
      </ul>
    </div>
  )
}

export default Menu
